console.log(`Hello World.`)

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],		
		kanto: ["Brock", "Misty"],
	},

	talk: function(){
		console.log(`Pikachu I choose you!`)
	},
	
}

console.log(trainer)
console.log(`Result of dot notation:`)
console.log(trainer.name)
console.log(`Result of bracket notation:`)
console.log(trainer[`pokemon`])
console.log(`Result of talk method:`)
trainer.talk()

function pokemon(name, lvl, hp, attack){
	this.name = name;
	this.level = lvl;
	this.health = hp;
	this.attack = attack;

	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`);
		console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`);
		target.health = target.health - this.attack;
		if(target.health <= 0){
			this.faint(target);
			}
		};

	this.faint = function(target){
			console.log(`${target.name} has fainted`)
			}
}

let pikachu = new pokemon("Pikachu",12,24,12)
let geodude = new pokemon("Geodude",8,16,8)
let mewtwo = new pokemon("Mewtow",100,200,100)
console.log(pikachu)
console.log(geodude)
console.log(mewtwo)

geodude.tackle(pikachu)
mewtwo.tackle(geodude)
console.log(geodude)




